// ==================================================
// Project Name  :  Fede
// File          :  Responsive CSS Base
// Version       :  1.0.0
// Last change   :  29 April 2019
// Author        :  ----------
// Developer:    :  Rakibul Islam Dewan
// ==================================================




(function($) {
  "use strict";




  
  $('select').niceSelect();





  // sidebar menu - start
  // --------------------------------------------------
  $(document).ready(function () {
    $('.close-btn, .overlay').on('click', function () {
      $('#sidebar-menu').removeClass('active');
      $('.overlay').removeClass('active');
    });

    $('#sidebar-collapse').on('click', function () {
      $('#sidebar-menu').addClass('active');
      $('.overlay').addClass('active');
    });
  });
  // sidebar menu - start
  // --------------------------------------------------




  
  // menu btn bg effect - start
  // --------------------------------------------------
  $(window).scroll(function() {
    if ($(this).scrollTop() > 80) {
      $('#sidebar-collapse').addClass('active');
    } else {
      $('#sidebar-collapse').removeClass('active');
    }
  });
  // menu btn bg effect - end
  // --------------------------------------------------




  
  // image popup - start
  // --------------------------------------------------
  $(document).ready(function() {
    $('.zoom-gallery').magnificPopup({
      delegate: '.popup-item',
      type: 'image',
      closeOnContentClick: false,
      closeBtnInside: false,
      mainClass: 'mfp-with-zoom mfp-img-mobile',
      image: {
        verticalFit: true,
        titleSrc: function(item) {
          return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
        }
      },
      gallery: {
        enabled: true
      },
      zoom: {
        enabled: true,
        duration: 300,
        opener: function(element) {
          return element.find('img');
        }
      }

    });
  });
  // image popup - end
  // --------------------------------------------------




  
  // quantity - start
  // --------------------------------------------------
  $(document).ready(function() {
    $('.add-btn').click(function () {
      if ($(this).prev().val() < 100) {
        $(this).prev().val(+$(this).prev().val() + 1);
      }
    });
    $('.sub-btn').click(function () {
      if ($(this).next().val() > 1) {
        if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
      }
    });
  });
  // quantity - end
  // --------------------------------------------------




  // sticky menu - start
  // --------------------------------------------------
  var headerId = $(".sticky-header");
  $(window).on('scroll' , function() {
    var amountScrolled = $(window).scrollTop();
    if ($(this).scrollTop() > 250) {
      headerId.removeClass("not-stuck");
      headerId.addClass("stuck");
    } else {
      headerId.removeClass("stuck");
      headerId.addClass("not-stuck");
    }
  });
  // sticky menu - end
  // --------------------------------------------------



  
})(jQuery);